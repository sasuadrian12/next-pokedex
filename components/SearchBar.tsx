"use client";

import { Loader2, Search } from "lucide-react";
import { Button } from "./ui/button";
import { Input } from "./ui/input";
import { Suspense, useRef, useState, useTransition } from "react";
import { useRouter, useSearchParams } from "next/navigation";

const SearchBar = () => {
  //switching cicle
  const [isSearching, startTransition] = useTransition();
  const searchParams = useSearchParams();
  const defaultQuery = searchParams.get("query") || "";
  const [query, setQuery] = useState<string>(defaultQuery);
  const router = useRouter();
  const inputRef = useRef<HTMLInputElement>(null);

  const search = () => {
    startTransition(() => {
      router.push(`/search?query=${query}`);
    });
  };

  return (
    <div className="relative h-14 z-10 rounded-md">
      <Input
        disabled={isSearching}
        onKeyDown={(e) => {
          if (e.key === "Escape") {
            inputRef?.current?.blur();
          }
          e.key === "Enter" && search();
        }}
        value={query}
        onChange={(e) => setQuery(e.target.value)}
        ref={inputRef}
        className="absolute inset-o h-full"
      />
      <Suspense>
        <Button
          disabled={isSearching}
          onClick={search}
          size={"sm"}
          className="absolute right-0 inset-y-0 h-full rounded-l-none"
        >
          {isSearching ? (
            <Loader2 className="h-6 w-6 animate-spin" />
          ) : (
            <Search className="h-6 w-6" />
          )}
        </Button>
      </Suspense>
    </div>
  );
};

export default SearchBar;
