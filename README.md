## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

This is a simple project using server components and query search methood.
Site: https://next-pokedex-gamma-murex.vercel.app/