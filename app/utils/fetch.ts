import axios from "axios";

export const fetchPokemonDetailsWithType = async (
  pokemonNamePrefix: any,
  type: string | null
) => {
  try {
    let url = `https://pokeapi.co/api/v2/pokemon?limit=1000`;
    if (type) {
      url = `https://pokeapi.co/api/v2/type/${type}`;
    }
    const response = await axios.get(url);
    const results = type ? response.data.pokemon : response.data.results;

    const filteredResults = results.filter((result: { name: string }) =>
      result.name.startsWith(pokemonNamePrefix.toLowerCase())
    );

    const pokemonDetailsPromises = filteredResults.map(
      async (result: { name: string; url: string }) => {
        const pokemonResponse = await axios.get(result.url);
        const pokemonData = pokemonResponse.data;
        const types = pokemonData.types.map(
          (type: { type: { name: string } }) => type.type.name
        );
        return { name: pokemonData.name, types };
      }
    );

    const pokemonDetailsWithType = await Promise.all(pokemonDetailsPromises);
    return pokemonDetailsWithType;
  } catch (error) {
    console.error("Error fetching data:", error);
    return [];
  }
};
