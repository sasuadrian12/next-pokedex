import BackButton from "@/components/BackButton";
import Image from "next/image";
import { notFound } from "next/navigation";

interface Pokemon {
  id: string;
  name: string;
  weight: string;
  height: string;
}

interface PageProps {
  params: {
    pokemonId: string;
  };
}

const Page = async ({ params }: PageProps) => {
  const { pokemonId } = params;

  try {
    const response = await fetch(
      `https://pokeapi.co/api/v2/pokemon/${pokemonId}`
    );
    if (!response.ok) {
      return notFound(); // Return a 404 page if the request fails
    }
    const data = await response.json();
    const { weight, name, height } = data as Pokemon;

    console.log(data);
    return (
      <div className="py-8 px-12 bg-white shadow-md rounded-lg flex flex-col justify-center items-center">
        <BackButton />
        <div className="mt-8">
          <h1 className="text-4xl font-bold text-gray-900">{name}</h1>
        </div>
        <div className="aspect-w-3 aspect-h-2 mt-6">
          <div className="relative w-full h-full overflow-hidden rounded-xl border border-gray-200">
            <Image
              width={220}
              height={180}
              alt="pokemon-image"
              src={`https://img.pokemondb.net/artwork/large/${name}.jpg`}
            />
          </div>
        </div>
        <div className="mt-6">
          <p className="text-xl font-bold text-gray-900">Weight: {weight}</p>
        </div>
        <div className="mt-4">
          <p className="text-xl font-bold text-gray-900">Height: {height}</p>
        </div>
      </div>
    );
  } catch (error) {
    console.error("Error fetching Pokemon:", error);
    return <div>Error fetching Pokemon</div>; // Render an error message if there's an error
  }
};

export default Page;
