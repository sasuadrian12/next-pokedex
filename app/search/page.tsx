import { redirect } from "next/navigation";
import Link from "next/link";
import Image from "next/image";
import { X } from "lucide-react"; 
import { fetchPokemonDetailsWithType } from "../utils/fetch";

interface PageProps {
  searchParams: {
    [key: string]: string | string[] | undefined;
  };
}

const Page = async ({ searchParams }: PageProps) => {
  const query = searchParams.query;
  if (Array.isArray(query) || !query) {
    return redirect("/");
  }

  const fetchData = async (type: string | null) => {
    return await fetchPokemonDetailsWithType(query, type);
  };

  const data = await fetchData(null); 

  if (data.length === 0) {
    return (
      <div className="text-center py-10 bg-white shadow-md rounded-b-md">
        <X className="mx-auto h-8 w-8 text-gray-400" />
        <h3 className="mt-2 text-sm font-semibold text-gray-900">No results</h3>
        <p className="mt-1 text-sm mx-auto max-w-prose text-gray-500">
          Sorry, we couldn't find any matches for {query}
        </p>
      </div>
    );
  }

  return (
    <>
      <ul className="flex flex-wrap justify-center py-8 bg-white">
        {data.map((pokemon: any, index: number) => (
          <Link key={index} href={`/pokemon/${pokemon.name}`}>
            <li className="flex flex-col items-center justify-center w-48 h-48 mx-4 my-4 bg-gray-100 rounded-lg overflow-hidden hover:bg-gray-200 transition-colors duration-300 cursor-pointer">
              <div className="flex items-center justify-center w-full h-36">
                <Image
                  width={120}
                  height={120}
                  alt="pokemon-image"
                  src={`https://img.pokemondb.net/artwork/large/${pokemon.name}.jpg`}
                />
              </div>
              <h1 className="text-center text-lg font-medium text-gray-900 mt-2">
                {pokemon.name}
              </h1>
              <p className="text-sm text-gray-600">
                Type: {pokemon.types.join(", ")}
              </p>
            </li>
          </Link>
        ))}
      </ul>
    </>
  );
};

export default Page;
